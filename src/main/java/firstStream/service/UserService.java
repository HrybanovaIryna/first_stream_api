package firstStream.service;

import firstStream.dto.ChangePasswordDto;
import firstStream.dto.UserDto;

public interface UserService {
    long registerNewAccount(UserDto userDto);

    boolean confirmActivation(String email, String code);

    boolean sendResetCode(String email);
    boolean checkConfirmResetCode(String email, String code);
    boolean changePassword(ChangePasswordDto changePasswordDto);

    boolean blockUser(int userId);
}

