package firstStream.service;

import firstStream.dto.CategoryDto;
import firstStream.dto.PageDto;
import firstStream.dto.SteamServiceDto;

import java.util.List;

public interface ServicesService {
    List<SteamServiceDto> getPopularServices(int n);
    PageDto<CategoryDto> getCategoryList(int page, int limit);
    boolean addCategory(CategoryDto categoryDto);
    boolean deleteCategory(Integer categoryId);
    boolean editCategory(CategoryDto categoryDto);
    PageDto<SteamServiceDto> getServicesList(int page, int limit);
    boolean addService(SteamServiceDto steamServiceDto);
    boolean deleteService(Integer steamServiceId);
    boolean editService(SteamServiceDto steamServiceDto);
    List<SteamServiceDto> getServicesFromCategory(Integer categoryId);
}
