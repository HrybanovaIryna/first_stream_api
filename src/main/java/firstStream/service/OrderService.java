package firstStream.service;

import firstStream.dto.OrderDto;
import firstStream.dto.SteamServiceDto;

import java.util.List;

public interface OrderService {
    List<OrderDto> getUserOrders(int userId, int page, int limit);
    List<OrderDto> getOrders(int page, int limit);
    boolean createOrder(int userId, List<SteamServiceDto> service);
}
