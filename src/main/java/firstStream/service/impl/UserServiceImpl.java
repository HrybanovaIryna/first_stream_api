package firstStream.service.impl;

import firstStream.dto.ChangePasswordDto;
import firstStream.dto.UserDto;
import firstStream.entity.Role;
import firstStream.entity.User;
import firstStream.exception.UserException;
import firstStream.repository.UserRepository;
import firstStream.service.ConfirmationService;
import firstStream.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ConfirmationService confirmationService;
    @Value("${confirmation.url}")
    private String confirmationUrl;
    @Value("${confirmation.reset.code.url}")
    private String conformationResetCodeUrl;

    private User getUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserException("User not found with login " + email));
    }

    private void checkLoginPassword(String email, String password) {
        if (isEmpty(email)) {
            throw new UserException("Incorrect email " + email);
        }

        if (isEmpty(password)) {
            throw new UserException("Incorrect password " + password);
        }
    }

    private boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    @Override
    @Transactional
    public long registerNewAccount(UserDto userDto) {
        if (Objects.nonNull(getUserByEmail(userDto.getEmail()))) {
            throw new UserException("User with email " + userDto.getEmail() + " is already registered.");
        }

        checkLoginPassword(userDto.getEmail(), userDto.getPassword());

        User user = new User(userDto.getEmail(), userDto.getPassword());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRoles(Collections.singleton(Role.USER));
        userRepository.save(user);
        confirmationService.sendCode(user.getEmail(), confirmationUrl);
        return user.getId();
    }

    @Override
    public boolean confirmActivation(String email, String code) {
        if (confirmationService.checkCode(email, code)) {
            User user = getUserByEmail(email);
            user.setEnabled(true);
            userRepository.save(user);
            return true;
        }

        return false;
    }

    @Override
    public boolean sendResetCode(String email) {
        getUserByEmail(email);
        confirmationService.sendCode(email, conformationResetCodeUrl);
        return true;
    }

    @Override
    public boolean checkConfirmResetCode(String email, String code) {
        return confirmationService.checkCodePresence(email, code);
    }

    @Override
    public boolean changePassword(ChangePasswordDto changePasswordDto) { //todo: add in a personal cabinet
        if (checkConfirmResetCode(changePasswordDto.getEmail(), changePasswordDto.getCode())) {
            User user = getUserByEmail(changePasswordDto.getEmail());

            user.setPassword(passwordEncoder.encode(changePasswordDto.getNewPassword()));
            userRepository.save(user);
        }
        return true;
    }

    @Override
    public boolean blockUser(int userId){
        return true; //todo
    }
}
