package firstStream.service.impl;

import firstStream.entity.ConfirmationCode;
import firstStream.exception.ConfirmationCodeException;
import firstStream.repository.ConfirmationCodeRepository;
import firstStream.service.ConfirmationService;
import firstStream.service.SendService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ConfirmationServiceImpl implements ConfirmationService {
    private static final int MAX_ATTEMPTS = 5;
    private static final int CODE_LIVE_TIME_DAYS = 1;
    private static final int SCHEDULED_RATE = 30 * 60 * 1000;
    private final SendService sendService;
    private final ConfirmationCodeRepository confirmationCodeRepository;

    @Override
    public void sendCode(String email, String url) {
        checkEmail(email);

        ConfirmationCode confirmationCode = confirmationCodeRepository.findByEmail(email)
                .orElseGet(() -> generateCode(email));

        sendService.sendEmail(email, "Activate your account", "To complete registration follow a link: <a href=\""
                + generateConfirmationLink(email, confirmationCode.getActivationCode(), url) + "\">Зареєструватися</a>");
    }

    private String generateConfirmationLink(String email, String confirmationCode, String url) {
        return url + "?email=" + email + "&code=" + confirmationCode;
    }

    private void checkEmail(String email) {
        if (Objects.isNull(email) || email.isEmpty()) {
            throw new ConfirmationCodeException("Incorrect email " + email);
        }
    }

    private ConfirmationCode generateCode(String email) {
        ConfirmationCode confirmationCode = new ConfirmationCode();
        confirmationCode.setActivationCode(UUID.randomUUID().toString());
        confirmationCode.setLiveTime(LocalDateTime.now().plusDays(CODE_LIVE_TIME_DAYS));
        confirmationCode.setEmail(email);
        confirmationCodeRepository.save(confirmationCode);
        return confirmationCode;
    }

    @Override
    public boolean checkCode(String email, String code) {
        checkEmail(email);

        ConfirmationCode confirmationCode = confirmationCodeRepository.findByEmail(email)
                .orElseThrow(() -> new ConfirmationCodeException("Incorrect code"));

        if (confirmationCode.getActivationCode().equals(code)) {
            confirmationCodeRepository.delete(confirmationCode);
            return true;
        }

        confirmationCode.setAttempts(confirmationCode.getAttempts() + 1);
        if (confirmationCode.getAttempts() > MAX_ATTEMPTS) {
            confirmationCodeRepository.delete(confirmationCode);
            throw new ConfirmationCodeException("Incorrect code");
        }

        confirmationCodeRepository.save(confirmationCode);

        return false;
    }

    @Override
    public boolean checkCodePresence(String email, String code) {
        checkEmail(email);

        return confirmationCodeRepository.findByEmail(email).isPresent();
    }

    @Transactional
    @Scheduled(fixedRate = SCHEDULED_RATE)
    public void deleteCode() {
        confirmationCodeRepository.deleteCode();
    }

}
