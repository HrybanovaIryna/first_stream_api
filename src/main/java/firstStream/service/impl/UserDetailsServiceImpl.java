package firstStream.service.impl;

import firstStream.entity.User;
import firstStream.exception.UserException;
import firstStream.repository.UserRepository;
import firstStream.service.UserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UserException("User not found with login " + email));
        String username = user.getEmail();
        String password = user.getPassword();
        Set<GrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> {
                    String roleName = "ROLE_" + role.name();
                    return new SimpleGrantedAuthority(roleName);
                })
                .collect(Collectors.toSet());
        return new org.springframework.security.core.userdetails.User(username, password, authorities);
    }
}
