package firstStream.service.impl;

import firstStream.dto.CategoryDto;
import firstStream.dto.PageDto;
import firstStream.dto.SteamServiceDto;
import firstStream.entity.Category;
import firstStream.entity.SteamService;
import firstStream.exception.ServiceException;
import firstStream.repository.CategoryRepository;
import firstStream.repository.ServiceRepository;
import firstStream.service.ServicesService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ServicesServiceImpl implements ServicesService {
    private final CategoryRepository categoryRepository;
    private final ModelMapper modelMapper;
    private final ServiceRepository serviceRepository;

    @Override
    public List<SteamServiceDto> getPopularServices(int n) {
        return serviceRepository.findSteamServicesByPopularity(n)
                .stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    @Override
    public PageDto<CategoryDto> getCategoryList(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit);
        Page<Category> categories = categoryRepository.findAll(pageable);
        return new PageDto<>(convert(categories.getContent()), categories.getTotalPages(), categories.getTotalElements());
    }

    private List<CategoryDto> convert(List<Category> content) {
        return content.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private CategoryDto convert(Category category) {
        return modelMapper.map(category, CategoryDto.class);
    }

    @Override //todo add security(ADMIN)
    public boolean addCategory(CategoryDto categoryDto) {
        categoryRepository.save(convertToCategory(categoryDto, new Category()));
        return true;
    }

    private Category convertToCategory(CategoryDto categoryDto, Category category) {
        modelMapper.map(categoryDto, category);
        return category;
    }

    @Override//todo add security(ADMIN)
    public boolean deleteCategory(Integer categoryId) {
        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ServiceException("Category is not found with id " + categoryId));
        categoryRepository.delete(category);
        return true;
    }

    @Override//todo add security(ADMIN)
    public boolean editCategory(CategoryDto categoryDto) {
        Category category = findCategoryById(categoryDto.getId());
        convertToCategory(categoryDto, category);
        categoryRepository.save(category);
        return true;
    }

    private Category findCategoryById(Integer id) {
        return categoryRepository.findById(id)
                .orElseThrow(() -> new ServiceException("Category is not found with id " + id));
    }

    @Override
    public PageDto<SteamServiceDto> getServicesList(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit);
        Page<SteamService> steamServices = serviceRepository.findAll(pageable);
        return new PageDto<>(convertToServiceDto(steamServices.getContent()), steamServices.getTotalPages(), steamServices.getTotalElements());
    }

    private List<SteamServiceDto> convertToServiceDto(List<SteamService> content) {
        return content.stream()
                .map(steamService -> modelMapper.map(steamService, SteamServiceDto.class))
                .collect(Collectors.toList());
    }

    @Override//todo add security(ADMIN)
    public boolean addService(SteamServiceDto steamServiceDto) {
        serviceRepository.save(convertToService(steamServiceDto, new SteamService()));
        return true;
    }

    private SteamService convertToService(SteamServiceDto steamServiceDto, SteamService service) {
        modelMapper.map(steamServiceDto, service);
        service.setCategory(findCategoryById(steamServiceDto.getCategoryId()));
        return service;
    }

    @Override//todo add security(ADMIN)
    public boolean deleteService(Integer steamServiceId) {
        SteamService steamService = findServiceById(steamServiceId);
        serviceRepository.delete(steamService);
        return true;
    }

    private SteamService findServiceById(Integer id) {
        return serviceRepository.findById(id)
                .orElseThrow(() -> new ServiceException("Service not found with id " + id));
    }

    @Override//todo add security(ADMIN)
    public boolean editService(SteamServiceDto steamServiceDto) {
        SteamService steamService = findServiceById(steamServiceDto.getId());
        convertToService(steamServiceDto, steamService);
        serviceRepository.save(steamService);
        return true;
    }

    @Override
    public List<SteamServiceDto> getServicesFromCategory(Integer categoryId) {
        return serviceRepository.findStreamServiceByCategoryId(categoryId).stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private SteamServiceDto convert(SteamService content) {
        return modelMapper.map(content, SteamServiceDto.class);
    }
}
