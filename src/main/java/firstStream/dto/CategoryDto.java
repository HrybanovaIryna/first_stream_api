package firstStream.dto;

import lombok.Data;

import java.util.Set;

@Data
public class CategoryDto {
    private Integer id;
    private String name;
    private Set<SteamServiceDto> steamServices;
}
