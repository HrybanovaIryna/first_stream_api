package firstStream.dto;

import lombok.Data;

@Data
public class FailResponse {
    private boolean fail = true;
    private final String message;
}
