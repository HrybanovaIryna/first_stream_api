package firstStream.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"password"})
public class UserDto {
    private String firstName;
    private String lastName;

    private String email;
    private String password;
}
