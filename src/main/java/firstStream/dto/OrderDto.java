package firstStream.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

@Data
public class OrderDto {
    private Integer id;
    private LocalDateTime time;
    private UserDto user;
    private Set<SteamServiceDto> services;
}
