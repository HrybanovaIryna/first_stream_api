package firstStream.dto;

import lombok.Data;

@Data
public class SteamServiceDto {
    private Integer id;
    private String name;
    private long price;
    private int categoryId;
}
