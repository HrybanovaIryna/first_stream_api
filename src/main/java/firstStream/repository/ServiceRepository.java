package firstStream.repository;

import firstStream.entity.SteamService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<SteamService, Integer> {

    @Query(value = "SELECT * " +
            "FROM steam_service s  " +
            "JOIN (SELECT COUNT(os.service_id) as amount, os.service_id as sid " +
            " FROM order_service os " +
            " GROUP BY os.service_id " +
            " ORDER BY amount DESC LIMIT :n) os_amount ON s.id = os_amount.sid", nativeQuery = true)
    List<SteamService> findSteamServicesByPopularity(int n);

    Page<SteamService> findAll(Pageable pageable);

    List<SteamService> findStreamServiceByCategoryId(Integer categoryId);
}
