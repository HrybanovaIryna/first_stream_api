package firstStream.repository;

import firstStream.entity.ConfirmationCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfirmationCodeRepository extends JpaRepository<ConfirmationCode, Long> {
    Optional<ConfirmationCode> findByEmail(String email);

    @Modifying
    @Query(value = "DELETE FROM confirmation_code WHERE live_time < now()", nativeQuery = true)
    void deleteCode();

}
