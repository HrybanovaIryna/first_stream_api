package firstStream.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table
@Data
@ToString(exclude = "steamServices")
@EqualsAndHashCode(exclude = "steamServices")
public class Category {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "category")
    private Set<SteamService> steamServices = new HashSet<>();
}
