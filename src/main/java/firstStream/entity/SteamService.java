package firstStream.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table
@Data
public class SteamService {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private long price;

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;
}
