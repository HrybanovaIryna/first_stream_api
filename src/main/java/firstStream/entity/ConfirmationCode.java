package firstStream.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table
@Data
public class ConfirmationCode {
    @Id
    @GeneratedValue
    private Integer id;
    private String activationCode;
    private int attempts;
    private LocalDateTime liveTime;
    private String email;
}
