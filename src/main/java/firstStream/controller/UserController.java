package firstStream.controller;

import firstStream.dto.ChangePasswordDto;
import firstStream.dto.UserDto;
import firstStream.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
@Log4j2
public class UserController {

    private final UserService userService;

    @GetMapping("/registration")
    public String getRegistrationPage() {
        return "registration.html";
    }

    @GetMapping("/login")
    public String getLoginPage() {
        return "login.html";
    }

    @ResponseBody
    @PostMapping(value = "/registration")
    public long registration(@RequestBody UserDto userDto) {
        log.info("User registration request: " + userDto);
        long response = userService.registerNewAccount(userDto);
        log.info("User registration response, id: " + response);
        return response;
    }

    @GetMapping(value = "/confirm")
    public String confirm(@RequestParam String email, @RequestParam String code) {
        log.info("User confirm request: email: " + email + " code " + code);
        boolean response = userService.confirmActivation(email, code);
        log.info("User confirm response: " + response);
        return "login";
    }

    @ResponseBody
    @GetMapping(value = "/sendResetCode")
    public boolean sendResetCode(@RequestParam String email) {
        log.info("Send reset code request: email: " + email);
        boolean response = userService.sendResetCode(email);
        log.info("Send reset code response: " + response);
        return response;
    }

    @ResponseBody
    @GetMapping(value = "/confirmResetCode")
    public boolean confirmResetCode(@RequestParam String email, @RequestParam String code) {
        log.info("Confirm password reset request" + email + " code " + code);
        boolean response = userService.checkConfirmResetCode(email, code);
        log.info("Confirm password reset response " + response);
        return response;
    }

    @ResponseBody
    @PostMapping(value = "/changePassword")
    public boolean changePassword(@RequestBody ChangePasswordDto changePasswordDto) {
        log.info("Change password request" + changePasswordDto.getEmail() + " code " + changePasswordDto.getCode());
        boolean response = userService.changePassword(changePasswordDto);
        log.info("Change password response " + response);
        return response;
    }
}
