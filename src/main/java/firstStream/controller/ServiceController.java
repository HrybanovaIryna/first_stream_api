package firstStream.controller;

import firstStream.dto.CategoryDto;
import firstStream.dto.PageDto;
import firstStream.dto.SteamServiceDto;
import firstStream.service.ServicesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/services")
@RequiredArgsConstructor
@Log4j2
public class ServiceController {
    private final ServicesService servicesService;

    @GetMapping("/main")
    public String getPricePage(Model model, @RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        PageDto<CategoryDto> categories = getCategoryList(page, limit);
        model.addAttribute("categories", categories);
        return "main.html";
    }

    @ResponseBody
    @GetMapping("/popularServices")
    public List<SteamServiceDto> getPopularServices(@RequestParam int n) {
        log.info("Popular services request: " + n);
        List<SteamServiceDto> response = servicesService.getPopularServices(n);
        log.info("Popular services response: " + response);
        return response;
    }

    @ResponseBody
    @GetMapping("/categories")
    public PageDto<CategoryDto> getCategoryList(@RequestParam int page, @RequestParam int limit) {
        log.info("Categories request: page: " + page + " limit: " + limit);
        PageDto<CategoryDto> response = servicesService.getCategoryList(page, limit);
        log.info("Categories response: " + response);
        return response;
    }

    @ResponseBody
    @PostMapping("/addCategory")
    public boolean addCategory(@RequestBody CategoryDto categoryDto) {
        log.info("Add category request: " + categoryDto);
        boolean response = servicesService.addCategory(categoryDto);
        log.info("Add category response: " + response);
        return response;
    }

    @ResponseBody
    @DeleteMapping("/deleteCategory")
    public boolean deleteCategory(@RequestParam Integer categoryId) {
        log.info("Delete category request: " + categoryId);
        boolean response = servicesService.deleteCategory(categoryId);
        log.info("Delete category response: " + response);
        return response;
    }

    @ResponseBody
    @PostMapping("/editCategory")
    public boolean editCategory(@RequestBody CategoryDto categoryDto) {
        log.info("Edit category request: " + categoryDto);
        boolean response = servicesService.editCategory(categoryDto);
        log.info("Edit category response: " + response);
        return response;
    }

    @ResponseBody
    @GetMapping("/getServices")
    public PageDto<SteamServiceDto> getServicesList(@RequestParam int page, @RequestParam int limit) {
        log.info("Get services request: page: " + page + " limit: " + limit);
        PageDto<SteamServiceDto> response = servicesService.getServicesList(page, limit);
        log.info("Get services response: " + response);
        return response;
    }

    @ResponseBody
    @PostMapping("/addService")
    public boolean addService(@RequestBody SteamServiceDto steamServiceDto) {
        log.info("Add service request: " + steamServiceDto);
        boolean response = servicesService.addService(steamServiceDto);
        log.info("Add service response: " + response);
        return response;
    }

    @ResponseBody
    @DeleteMapping("/deleteService")
    public boolean deleteService(@RequestParam Integer steamServiceId) {
        log.info("Delete service request: " + steamServiceId);
        boolean response = servicesService.deleteService(steamServiceId);
        log.info("Delete service response: " + response);
        return response;
    }

    @ResponseBody
    @PostMapping("/editService")
    public boolean editService(@RequestBody SteamServiceDto steamServiceDto) {
        log.info("Edit service request: " + steamServiceDto);
        boolean response = servicesService.editService(steamServiceDto);
        log.info("Edit service response: " + response);
        return response;
    }

    @ResponseBody
    @GetMapping("/getServicesFromCategory")
    public List<SteamServiceDto> getServicesFromCategory(@RequestParam Integer categoryId) {
        log.info("Get services from category request: " + categoryId);
        List<SteamServiceDto> response = servicesService.getServicesFromCategory(categoryId);
        log.info("Get services from category response: " + response);
        return response;
    }

}
