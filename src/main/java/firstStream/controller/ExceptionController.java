package firstStream.controller;

import firstStream.dto.FailResponse;
import firstStream.exception.UserException;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Log4j2
public class ExceptionController {

    @ExceptionHandler(UserException.class)
    public FailResponse handleUserException(UserException e) {
        log.error("Was caught UserException.", e);
        return new FailResponse(e.getMessage());
    }
}
