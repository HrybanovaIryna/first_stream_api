package firstStream.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

    @GetMapping("/contacts")
    public String getContactsPage() {
        return "contacts.html";
    }

    @GetMapping("/personalAccount")
    public String getPersonalAccountPage() {
        return "personalAccount.html";
    }

    @GetMapping("/services")
    public String getServicesPage() {
        return "services.html";
    }
}
