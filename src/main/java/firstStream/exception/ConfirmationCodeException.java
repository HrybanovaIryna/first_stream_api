package firstStream.exception;

public class ConfirmationCodeException extends RuntimeException {
    public ConfirmationCodeException(String message) {
        super(message);
    }
}
